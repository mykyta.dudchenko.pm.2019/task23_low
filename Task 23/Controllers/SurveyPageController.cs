﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Task_23.Controllers
{
    public class SurveyPageController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(FormCollection form)
        {
            string[] keys = new string[] { "Banana", "Apple", "Melon", "Watermelon", "Orange" };
            ViewBag.person = form["Person"].ToString();
            ViewBag.surname = form["Surname"].ToString();
            ViewBag.framework = form["framework"].ToString();
            ViewBag.language = form["languages"].ToString();
            ViewBag.fruits = new List<string>();
            foreach (var key in keys)
            {
                if (form[key] == "true,false") 
                {
                    ViewBag.fruits.Add(key);
                }
            }
            if (ViewBag.fruits.Count == 0)
                ViewBag.fruits.Add("There is no fruits have been chosen");
            return View("Submitted");
        }
    }
}